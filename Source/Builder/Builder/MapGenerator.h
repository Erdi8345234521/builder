// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CityGenerator.h"
#include "GameFramework/Actor.h"
#include "CanGenerateCity.h"
#include "GenerationDirector.h"
#include "MapGenerator.generated.h"

/*
 * Some actor that is the centre point for all cities to be built around.
 */
UCLASS()
class BUILDER_API AMapGenerator : public AActor
{
	GENERATED_BODY()

public:
	/** Creates two cities and quests for them */
	void GenerateMap();

	void BeginPlay() override;
	
private:
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Cities", meta = (AllowPrivateAccess = "true"))
		TArray<AActor*> Cities;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Quests", meta = (AllowPrivateAccess = "true"))
		TArray<UObject*> Quests;
};

template<class TClass>
ICanGenerateCity* CastToCanGenerateCity(TClass* Object)
{
	return static_cast<ICanGenerateCity*>(Object->GetInterfaceAddress(UCanGenerateCity::StaticClass()));
}

inline void AMapGenerator::BeginPlay()
{
	Super::BeginPlay();

	GenerateMap();
}

inline void AMapGenerator::GenerateMap()
{
	//Creating necromantic city
	auto NecroGenerator = NewObject<UNecroCityGenerator>(this);
	if (NecroGenerator)
	{
		UKismetSystemLibrary::PrintString(this, "GenerateNecroCity", true, true, FLinearColor(0.0, 0.66, 1.0), 10.f);
		UGenerationDirector::GenerateCity(NecroGenerator);
		Cities.Emplace(Cast<AActor>(NecroGenerator->GetResult()));
	}
	
	//Create necromantic city quests
	auto QuestGenerator = NewObject<UCityQuestGenerator>(this);
	if(QuestGenerator)
	{
		UKismetSystemLibrary::PrintString(this, "QuestGenerate in NecroCity", true, true, FLinearColor(0.0, 0.66, 1.0), 10.f);

		UGenerationDirector::GenerateCity(QuestGenerator);
		Quests.Emplace(QuestGenerator->GetResult());
	}	
	
	//Create elven city
	auto ElvenGenerator = NewObject<UElvenCityGenerator>(this);
	if(ElvenGenerator)
	{
		UKismetSystemLibrary::PrintString(this, "GenerateElvenCity", true, true, FLinearColor(0.0, 0.66, 1.0), 10.f);

		UGenerationDirector::GenerateCity(ElvenGenerator);
		Cities.Emplace(Cast<AActor>(ElvenGenerator->GetResult()));
	}

	//Create elven city quests
	if(QuestGenerator)
	{
		UKismetSystemLibrary::PrintString(this, "QuestGenerate in ElvenCity", true, true, FLinearColor(0.0, 0.66, 1.0), 10.f);

		UGenerationDirector::GenerateCity(QuestGenerator);
		Quests.Emplace(QuestGenerator->GetResult());
	}	
}